# webEOS-site-operator

## Prerequisites to run in OpenShift 3.11
- operator-sdk version: v0.10.1
- go version: go1.13

For dependency management we are using `go mod vendor`

## Development
This WebEOS operator includes the CRD and Custom Controller for WebEOS site provisioning and management.
Generating the project using Operator SDK
```
operator-sdk new webeos-site-operator --dep-manager=dep
```
Adding the WebEOS Custom Controller
```
operator-sdk add controller --api-version=webeos.webservices.cern.ch/v1alpha1 --kind=WebeosSite
```

Modify types as desired in `/root/go/src/gitlab.cern.ch/webeos-site-operator/pkg/apis/webeos/v1alpha1/webeossite_types.go` and
generate the CRDsa apis
Note: This command must be run every time the api (spec and status) for a custom resource type is updated.
```
operator-sdk generate k8s
```

Generates all Custom Resource Definition 
```
operator-sdk generate openapi
```

Create a file names `go.mod` with the dependencies defined
and execute
```
go mod vendor
```

Build go module for controller
```
go build -o /webeos-site-operator -gcflags all=-trimpath=/root/go/src/gitlab.cern.ch -asmflags all=-trimpath=/root/go/src/gitlab.cern.ch gitlab.cern.ch/webeos-site-operator/cmd/manager
```
