set -e

# Sometimes given that operators take a bit more to start reconciling properly or simply take a bit more to properly reconcile an object
# this function will try to make the test assertion 100 times and if at the end we do not obtain the expected behaviour that's because something is wrong
function try-to-execute () {
    sleep 1
    for run in {1..100}; do
        RESULT=$(eval $1 2>&1)
        if test "$RESULT" == "$2" ; then
            return 0
        fi
    done
    echo "Command "$1" failed to run more than 100 times"  >&2
    exit 1
}

echo Creating a WebEOS site route and verifying all the necessary things
oc create -f tests/resources/webeos-site.yaml

echo Check if the route was created with the name of the CR created
try-to-execute "oc get route mysite -o json | jq -r '.metadata.name'" "mysite"
echo -e "OK\n"

echo Description related tests
echo Check if the route was created with the proper description
try-to-execute "oc get route mysite -o json | jq -r '.metadata.labels.app'" "webeos-base"
echo -e "OK\n"

echo Check if we can update the Route SitePath successfully after updating in CR with invalid SitePath
oc patch webeossite mysite -p '{"spec":{"ownerProvidedEosFolder":{"sitePath":"/eos/../webservices/webeos"}}}' --type='merge'
try-to-execute "oc get route mysite -o json | jq -r '.metadata.name'" "mysite"
echo -e "OK\n" # todo

echo Check if we can update the Route SitePath successfully after updating in CR
oc patch webeossite mysite -p '{"spec":{"ownerProvidedEosFolder":{"sitePath":"/eos/webservices/webeos"}}}' --type='merge'
try-to-execute "oc get route mysite -o json | jq -r '.metadata.annotations.\"webservices.cern.ch/document-root\"'" "/eos/webservices/webeos"
echo -e "OK\n"

echo Check if targetPort exists
try-to-execute "oc get route mysite -o json | jq -r '.spec.port.targetPort'" "web"
echo -e "OK\n"

echo Deleting webeossite custom resource
oc delete -f tests/resources/webeos-site.yaml

# We need to switch to set +e due to the error output of the commands we are executing
set +e
echo Check if the CR was deleted
try-to-execute "oc get webeossite mysite -o json" "Error from server (NotFound): webeossites.webeos.webservices.cern.ch \"mysite\" not found"
echo -e "OK\n"

echo Check if the CR deleted the route
try-to-execute "oc get route mysite -o json | jq -r '.metadata.name'" "Error from server (NotFound): routes.route.openshift.io \"mysite\" not found"
echo -e "OK\n"
