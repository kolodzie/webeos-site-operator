package controller

import (
	"gitlab.cern.ch/webeos-site-operator/pkg/controller/webeossite"
)

func init() {
	// AddToManagerFuncs is a list of functions to create controllers and add them to a manager.
	AddToManagerFuncs = append(AddToManagerFuncs, webeossite.Add)
}
