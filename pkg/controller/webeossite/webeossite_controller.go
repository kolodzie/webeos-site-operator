package webeossite

import (
	"context"
	"flag"

	"github.com/go-logr/logr"
	webeosv1alpha1 "gitlab.cern.ch/webeos-site-operator/pkg/apis/webeos/v1alpha1"

	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"

	goCli "sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"

	//"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	utils "gitlab.cern.ch/webeos-site-operator/pkg/utils"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/controller-runtime/pkg/source"

	"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/util/intstr"

	routev1 "github.com/openshift/api/route/v1"
	// routev1 "github.com/openshift/client-go/route/clientset/versioned/typed/route/v1"
)

const (
	// DocRootAnnotation Document Root Annotation key for WebEOS site Route
	DocRootAnnotation   = "webservices.cern.ch/document-root"
	webeosSiteFinalizer = "finalizer.controller-webeossite.webservices.cern.ch"
)

// List of all router shards available in the cluster - passed as a flag to the operator
var shards string

var log = logf.Log.WithName("controller_webeossite")

func init() {
	flag.StringVar(&shards, "shards", "", "List of available router shards")
}

// Add creates a new WebeosSite Controller and adds it to the Manager. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ReconcileWebeosSite{client: mgr.GetClient(), scheme: mgr.GetScheme()}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("webeossite-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to primary resource WebeosSite
	err = c.Watch(&source.Kind{Type: &webeosv1alpha1.WebeosSite{}}, &handler.EnqueueRequestForObject{})
	if err != nil {
		return err
	}

	return nil
}

// blank assignment to verify that ReconcileWebeosSite implements reconcile.Reconciler
var _ reconcile.Reconciler = &ReconcileWebeosSite{}

// ReconcileWebeosSite reconciles a WebeosSite object
type ReconcileWebeosSite struct {
	// This client, initialized using mgr.Client() above, is a split client
	// that reads objects from the cache and writes to the apiserver
	client goCli.Client
	scheme *runtime.Scheme
}

// Reconcile reads that state of the cluster for a WebeosSite object and makes changes based on the state read
// and what is in the WebeosSite.Spec
// Note: The Controller will requeue the Request to be processed again if the returned error is non-nil or
// Result.Requeue is true, otherwise upon completion it will remove the work from the queue.
func (r *ReconcileWebeosSite) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	reqLogger := log.WithValues("Request.Namespace", request.Namespace, "Request.Name", request.Name)
	reqLogger.Info("Reconciling WebeosSite")

	// Fetch the WebeosSite instance
	instance := &webeosv1alpha1.WebeosSite{}
	err := r.client.Get(context.TODO(), request.NamespacedName, instance)
	if err != nil {
		if errors.IsNotFound(err) {
			// Request object not found, could have been deleted after reconcile request.
			// Owned objects are automatically garbage collected. For additional cleanup logic use finalizers.
			// Return and don't requeue
			return reconcile.Result{}, nil
		}
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}

	// Check if the CR was marked to be deleted
	if instance.GetDeletionTimestamp() != nil {
		if err := RemoveFinalizer(r.client, reqLogger, instance); err != nil {
			return reconcile.Result{}, err
		}
		return reconcile.Result{}, nil
	}

	if err := AddFinalizer(r.client, reqLogger, instance); err != nil {
		return reconcile.Result{}, err
	}

	if needToCreateRoute(instance) {
		if err := createRoute(r.client, reqLogger, instance); err != nil {
			updateCRwithError(r.client, reqLogger, instance, err.Error())
			return reconcile.Result{}, err
		}
		reqLogger.Info("Created new Route")
		return reconcile.Result{}, nil
	}

	route, err := getRoute(r.client, reqLogger, instance)
	if err != nil {
		if errors.IsNotFound(err) {
			// Route take a bit to be created until there reconsile without sending errors of not found
			return reconcile.Result{Requeue: true}, nil
		}
		return reconcile.Result{}, err
	}

	annotations := route.GetAnnotations()
	// Update Route Annotations
	if !utils.CorrectPair(DocRootAnnotation, instance.Spec.OwnerProvidedEosFolder.SitePath, annotations) {
		if err := updateRoute(r.client, reqLogger, route, instance); err != nil {
			updateCRwithError(r.client, reqLogger, instance, err.Error())
			return reconcile.Result{}, err
		}

		reqLogger.Info("Updated Route Annotations")
		return reconcile.Result{}, nil
	}

	// Update CR Status to Sucessfull Reconcile
	if instance.Status.ProvisioningStatus != webeosv1alpha1.Created {
		successfulReconcile(r.client, reqLogger, instance)
	}

	// assign random router shard
	if err := assignRouterShard(r.client, reqLogger, instance); err != nil {
		return reconcile.Result{}, err
	}

	// assign webeos deployment based on some predicates
	if err := assignWebEOSDeployment(r.client, reqLogger, instance); err != nil {
		return reconcile.Result{}, err
	}

	reqLogger.Info("WebeosSite reconciled", "Name", instance.Name)

	// Route already exists - don't requeue
	//reqLogger.Info("Skip reconcile: Route already exists", "Route.Namespace", found.Namespace, "Route.Name", found.Name)
	return reconcile.Result{}, nil
}

func needToCreateRoute(instance *webeosv1alpha1.WebeosSite) bool {

	if instance.Status.ProvisioningStatus == webeosv1alpha1.NonePS || instance.Status.ProvisioningStatus == webeosv1alpha1.ProvisioningFailed {
		return true
	}
	return false
}

// newRouteForCR returns a route esquema
func newRouteForCR(reqLogger logr.Logger, cr *webeosv1alpha1.WebeosSite) *routev1.Route {
	if !utils.ValidatePath(cr.Spec.OwnerProvidedEosFolder.SitePath) {
		reqLogger.Info("EOS SitePath is not valid contains special chars, hence can't create site")
		return nil
	}

	labels := map[string]string{
		"app": "webeos-base",
	}
	annotations := map[string]string{
		DocRootAnnotation: cr.Spec.OwnerProvidedEosFolder.SitePath,
	}

	return &routev1.Route{
		// TypeMeta: metav1.TypeMeta{
		// 	APIVersion: "route.openshift.io/v1",
		// 	Kind:       "Route",
		// },
		ObjectMeta: metav1.ObjectMeta{
			Name:        cr.Name,
			Namespace:   cr.Namespace,
			Labels:      labels,
			Annotations: annotations,
		},
		Spec: routev1.RouteSpec{
			TLS: &routev1.TLSConfig{
				InsecureEdgeTerminationPolicy: "Redirect",
				Termination:                   "edge",
			},
			To: routev1.RouteTargetReference{
				Kind: "Service",
				Name: "webeos-base",
			},
			Host: cr.Spec.HostName,
			Port: &routev1.RoutePort{
				TargetPort: intstr.FromString("web"),
			},
		},
	}
}

// getRoute gets route
func getRoute(client goCli.Client, reqLogger logr.Logger, instance *webeosv1alpha1.WebeosSite) (route *routev1.Route, err error) {
	route = &routev1.Route{}
	key := types.NamespacedName{
		Name:      instance.Name,
		Namespace: instance.Namespace,
	}

	if err = client.Get(context.TODO(), key, route); err != nil {
		return route, err
	}
	return route, nil
}

// updateRoute function updates the route's annotations
func updateRoute(client goCli.Client, reqLogger logr.Logger, route *routev1.Route, cr *webeosv1alpha1.WebeosSite) (err error) {
	route.Annotations["webservices.cern.ch/document-root"] = cr.Spec.OwnerProvidedEosFolder.SitePath

	if err := client.Update(context.TODO(), route); err != nil {
		reqLogger.Error(err, "Failed to update route annotation", "route.name", route.Name)
		return err
	}
	return nil
}

func createRoute(client goCli.Client, reqLogger logr.Logger, instance *webeosv1alpha1.WebeosSite) (err error) {
	r := newRouteForCR(reqLogger, instance)
	if r == nil {
		return nil
	}

	// create the route resource
	if err := client.Create(context.TODO(), r); err != nil {
		reqLogger.Error(err, "Failed to create route")
		return err
	}

	// Update the Status of the CR
	instance.Status.ProvisioningStatus = webeosv1alpha1.Created
	if err := client.Status().Update(context.TODO(), instance); err != nil {
		reqLogger.Error(err, "Failed to create WebeosSite status on createRoute", "webeossite.name", instance.Name)
		return err
	}
	return nil
}

// deleteRoute deletes the route
func deleteRoute(client goCli.Client, reqLogger logr.Logger, instance *webeosv1alpha1.WebeosSite) error {
	route, err := getRoute(client, reqLogger, instance)
	if errors.IsNotFound(err) {
		return nil
	}
	if err = client.Delete(context.TODO(), route); err != nil {
		return err
	}
	return nil
}

// RemoveFinalizer will execute the necessary actions to clean up resources created by
// the existance of the CR and finally will remove the finalizer from WebeosSite CR so
// that it can be deleted
func RemoveFinalizer(client goCli.Client, reqLogger logr.Logger, instance *webeosv1alpha1.WebeosSite) error {
	if utils.Contains(instance.GetFinalizers(), webeosSiteFinalizer) {
		// Here we ignore erros of not found since we are trying to cleanup things
		// TODO this might not be necessary since the resource is namespaced
		if err := deleteRoute(client, reqLogger, instance); err != nil && !errors.IsNotFound(err) {
			return err
		}

		// Remove webeosSiteFinalizer. Once all finalizers have been
		// removed, the object will be deleted.
		instance.SetFinalizers(utils.Remove(instance.GetFinalizers(), webeosSiteFinalizer))
		err := client.Update(context.TODO(), instance)
		if err != nil {
			reqLogger.Error(err, "Failed to update a CR when removing the finalizer")
			return err
		}
	}
	return nil
}

// AddFinalizer will add a finalizer to the WebeosSite CR so that
// we can delete it appropriately
func AddFinalizer(client goCli.Client, reqLogger logr.Logger, instance *webeosv1alpha1.WebeosSite) error {
	if utils.Contains(instance.GetFinalizers(), webeosSiteFinalizer) {
		return nil
	}

	reqLogger.Info("Adding Finalizer to the WebeosSite")
	instance.SetFinalizers(append(instance.GetFinalizers(), webeosSiteFinalizer))

	err := client.Update(context.TODO(), instance)
	if err != nil {
		reqLogger.Error(err, "Failed to update a CR with a finalizer")
		return err
	}

	// Initialize the Status of the CR
	instance.Status = webeosv1alpha1.WebeosSiteStatus{
		ProvisioningStatus: webeosv1alpha1.NonePS,
		ProvisioningError:  webeosv1alpha1.NoneRS,
		ErrorMessage:       "",
		ServerDetails:      &webeosv1alpha1.ServerDetails{},
	}

	if err := client.Status().Update(context.TODO(), instance); err != nil {
		reqLogger.Error(err, "Failed to update WebeosSite status", "webeossite.name", instance.Name)
		return err
	}

	return nil
}

func updateCRwithError(client goCli.Client, reqLogger logr.Logger, instance *webeosv1alpha1.WebeosSite, errorMessage string) {
	instance.Status.ProvisioningStatus = webeosv1alpha1.ProvisioningFailed
	instance.Status.ProvisioningError = webeosv1alpha1.NoneRS
	instance.Status.ErrorMessage = errorMessage
	if err := client.Status().Update(context.TODO(), instance); err != nil {
		reqLogger.Error(err, "Failed to update WebeosSite with error message", "error message", errorMessage)
	}
}

func successfulReconcile(client goCli.Client, reqLogger logr.Logger, instance *webeosv1alpha1.WebeosSite) {
	instance.Status.ProvisioningStatus = webeosv1alpha1.Created
	instance.Status.ProvisioningError = webeosv1alpha1.NoneRS
	instance.Status.ErrorMessage = ""

	if err := client.Status().Update(context.TODO(), instance); err != nil {
		reqLogger.Error(err, "WebeosSite reconciled without problem, but WebeosSite status could not be udpated")
	}
}
