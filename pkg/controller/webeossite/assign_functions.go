package webeossite

import (
	"context"
	"errors"
	"math/rand"
	"strings"
	"time"

	"github.com/go-logr/logr"
	webeosv1alpha1 "gitlab.cern.ch/webeos-site-operator/pkg/apis/webeos/v1alpha1"
	utils "gitlab.cern.ch/webeos-site-operator/pkg/utils"
	appsv1 "k8s.io/api/apps/v1"
	goCli "sigs.k8s.io/controller-runtime/pkg/client"
)

func getAvailableRouterShards() []string {
	if shards == "" {
		return nil
	}
	return strings.Split(shards, ",")
}

func assignRouterShard(client goCli.Client, reqLogger logr.Logger, instance *webeosv1alpha1.WebeosSite) error {
	if instance.Status.ServerDetails.AssignedRouterShard != "" {
		return nil
	}

	shards := getAvailableRouterShards()
	if shards == nil {
		return errors.New("specify a valid list of available router shards with the --shards flag")
	}

	instance.Status.ServerDetails.AssignedRouterShard = utils.RandomStrElement(shards)
	if err := client.Status().Update(context.TODO(), instance); err != nil {
		reqLogger.Error(err, "Failed to update WebEosSite.Status.RouterShard")
		return err
	}
	return nil
}

func getAllWebeosDeployments(client goCli.Client) (*appsv1.DeploymentList, error) {
	webEosDeployList := &appsv1.DeploymentList{}
	labels := map[string]string{"webeos.cern.ch/place-new-webeos-sites": "true"}
	if err := client.List(context.TODO(), goCli.MatchingLabels(labels), webEosDeployList); err != nil {
		return nil, err
	}
	return webEosDeployList, nil
}

func matchBestDeployment(deployments *appsv1.DeploymentList, instance *webeosv1alpha1.WebeosSite) appsv1.Deployment {
	instanceEosPath := instance.Spec.OwnerProvidedEosFolder.SitePath
	eligibleDeployments := []appsv1.Deployment{}

	for _, deployment := range deployments.Items {
		if isDeploymentEligibleForAssignment(deployment, instanceEosPath) {
			eligibleDeployments = append(eligibleDeployments, deployment)
		}
	}

	instancePathParts := strings.Split(instanceEosPath, "/")
	bestScore := 0
	scores := make([]int, len(eligibleDeployments))

	for deployIndex, deployment := range eligibleDeployments {
		score := 0
		pathPrefixParts := strings.Split(deployment.Annotations["webeos.cern.ch/webeos-site-path-prefix"], "/")

		for index := 0; index < len(pathPrefixParts); index++ {
			if part := pathPrefixParts[index]; part == instancePathParts[index] {
				score++
			}
		}

		scores[deployIndex] = score
		if score > bestScore {
			bestScore = score
		}
	}

	highestScoreDeployments := []appsv1.Deployment{}
	for index, deployment := range eligibleDeployments {
		if scores[index] == bestScore {
			highestScoreDeployments = append(highestScoreDeployments, deployment)
		}
	}

	if len(highestScoreDeployments) == 1 {
		return highestScoreDeployments[0]
	}

	s := rand.NewSource(time.Now().Unix())
	r := rand.New(s)
	return highestScoreDeployments[r.Intn(len(highestScoreDeployments))]
}

func isDeploymentEligibleForAssignment(deployment appsv1.Deployment, eosPath string) bool {
	eosSitePathPrefix := deployment.Annotations["webeos.cern.ch/webeos-site-path-prefix"]
	return strings.HasPrefix(eosPath, eosSitePathPrefix)
}

func assignWebEOSDeployment(client goCli.Client, reqLogger logr.Logger, instance *webeosv1alpha1.WebeosSite) error {
	if instance.Status.ServerDetails.AssignedDeploymentShard != "" {
		return nil
	}

	availableDeployments, err := getAllWebeosDeployments(client)
	if err != nil {
		return err
	} else if len(availableDeployments.Items) == 0 {
		return errors.New("There is no available deployments at the moment")
	}

	bestDeployment := matchBestDeployment(availableDeployments, instance)
	instance.Status.ServerDetails.AssignedDeploymentShard = bestDeployment.Name

	if err := client.Status().Update(context.TODO(), instance); err != nil {
		reqLogger.Error(err, "Failed to update WebEosSite.Status.WebEOSDeployment")
		return err
	}
	return nil
}
