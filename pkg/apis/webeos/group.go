// Package webeos contains webeos API versions.
//
// This file ensures Go source parsers acknowledge the webeos package
// and any child packages. It can be removed if any other Go source files are
// added to this package.
package webeos
