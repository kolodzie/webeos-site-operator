package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// WebeosSiteSpec defines the desired state of WebeosSite
// +k8s:openapi-gen=true
type WebeosSiteSpec struct {

	// Site description (e.g. "Hello mysite name")
	// +kubebuilder:validation:MaxLength=255
	// +kubebuilder:validation:MinLength=1
	SiteDescription string `json:"siteDescription"`

	// Site type, possible values: centrallyManagedEosFolder, ownerProvidedEosFolder
	// +kubebuilder:validation:Enum=centrallyManagedEosFolder,ownerProvidedEosFolder
	Type string `json:"type"`

	// TODO: investigate how we can make dependent CentrallyManagedEosFolder and OwnerProvidedEosFolder
	// e.g. OwnerProvidedEosFolder notEmpty requiring CentrallyManagedEosFolder
	// WebEOS site path
	// +kubebuilder:validation:MaxLength=25
	// +kubebuilder:validation:ExclusiveMinimum=true
	OwnerProvidedEosFolder *OwnerProvidedEosFolder `json:"ownerProvidedEosFolder,omitempty"`

	// Webeos centrally managed Eos folder
	// +kubebuilder:validation:MaxLength=25
	// +kubebuilder:validation:ExclusiveMinimum=true
	CentrallyManagedEosFolder *CentrallyManagedEosFolder `json:"centrallyManagedEosFolder,omitempty"`

	// This field if True adds the sitename in URL
	// +kubebuilder:validation:Enum=true,false
	// +kubebuilder:validation:ExclusiveMinimum=true
	UseLegacyBaseURL bool `json:"useLegacyBaseURL,omitempty"`

	// Site configuration of EOS paths
	Configuration []WebeosSiteConfigurationPath `json:"configuration,omitempty"`

	// Server details of the site, which routerShard and deploymentShrad are using
	ServerDetails *ServerDetails `json:"serverDetails,omitempty"`

	// HostName to be used while creating the Route
	// +kubebuilder:validation:MaxLength=255
	// +kubebuilder:validation:MinLength=1
	HostName string `json:"hostName,omitempty"`
}

// WebeosSiteStatus defines the observed state of WebeosSite
// +k8s:openapi-gen=true
type WebeosSiteStatus struct {
	// Return code of the last operation
	// +kubebuilder:validation:MaxLength=25
	// +kubebuilder:validation:MinLength=1
	ProvisioningStatus ProvisioningStatus `json:"provisioningStatus"`

	// Error reason in case of error
	// +kubebuilder:validation:MaxLength=25
	// +kubebuilder:validation:MinLength=1
	ProvisioningError ProvisioningError `json:"provisioningError"`

	// Error message in case of error
	// +kubebuilder:validation:MaxLength=25
	ErrorMessage string `json:"errorMessage"`

	// Server details of the site, which routerShard and deploymentShrad are using
	// +kubebuilder:validation:MaxLength=25
	// +kubebuilder:validation:ExclusiveMinimum=true
	ServerDetails *ServerDetails `json:"serverDetails,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// WebeosSite is the Schema for the webeossites API
// +k8s:openapi-gen=true
// +kubebuilder:subresource:status
type WebeosSite struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   WebeosSiteSpec   `json:"spec,omitempty"`
	Status WebeosSiteStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// WebeosSiteList contains a list of WebeosSite
type WebeosSiteList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []WebeosSite `json:"items"`
}

type CentrallyManagedEosFolder struct {
	Editors map[string]string `json:"editors,omitempty"`
}

type OwnerProvidedEosFolder struct {
	// +kubebuilder:validation:MaxLength=255
	// +kubebuilder:validation:ExclusiveMinimum=true
	SitePath string `json:"sitePath,omitempty"`
}

type ServerDetails struct {
	// +kubebuilder:validation:MaxLength=25
	// +kubebuilder:validation:ExclusiveMinimum=true
	AssignedRouterShard string `json:"assignedRouterShard,omitempty"`
	// +kubebuilder:validation:MaxLength=25
	// +kubebuilder:validation:ExclusiveMinimum=true
	AssignedDeploymentShard string `json:"assignedDeploymentShard,omitempty"`
}

// WebeosSiteConfigurationPath contains a configuration path
type WebeosSiteConfigurationPath struct {
	// +kubebuilder:validation:MaxLength=255
	// +kubebuilder:validation:ExclusiveMinimum=true
	Path string `json:"path,omitempty"`
	// +kubebuilder:validation:Enum=true,false
	// +kubebuilder:validation:ExclusiveMinimum=true
	CgiEnabled bool `json:"cgiEnabled,omitempty"`
}

//ProvisioningStatus contain the status if the site was provisioned
type ProvisioningStatus string

const (
	// NonePS none state pattern to be sure of enums initialization.
	NonePS             ProvisioningStatus = "none"
	Creating           ProvisioningStatus = "creating"
	Created            ProvisioningStatus = "created"
	ProvisioningFailed ProvisioningStatus = "provisioningFailed"
)

// ProvisioningError of the last operation
type ProvisioningError string

const (
	NoneRS                ProvisioningError = "none"
	InvalidName           ProvisioningError = "invalidName"
	RouteCreationFailed   ProvisioningError = "routeCreationFailed"
)

func init() {
	SchemeBuilder.Register(&WebeosSite{}, &WebeosSiteList{})
}
