// Package v1alpha1 contains API Schema definitions for the webeos v1alpha1 API group
// +k8s:deepcopy-gen=package,register
// +groupName=webeos.webservices.cern.ch
package v1alpha1
