package utils

import (
	"math/rand"
	"strings"
	"time"
)

// Contains checks if a string is in an array
func Contains(list []string, s string) bool {
	for _, v := range list {
		if v == s {
			return true
		}
	}
	return false
}

// Remove removes a string from an array of strings
func Remove(list []string, s string) []string {
	for i, v := range list {
		if v == s {
			list = append(list[:i], list[i+1:]...)
		}
	}
	return list
}

// RandomStrElement returns a random element from the array
func RandomStrElement(list []string) string {
	s := rand.NewSource(time.Now().Unix())
	r := rand.New(s)
	return list[r.Intn(len(list))]
}

// CorrectPair checks if dictionary contains a key or if it has the proper value set
func CorrectPair(key string, value string, dic map[string]string) bool {
	if dic[key] == "" || dic[key] != value {
		return false
	}
	return true
}

// validatePath checks if the mentioned EOS path by the user is valid and
// is not a critical path on the webserver container similar to `/eos/../etc/...`
func ValidatePath(s string) bool {
	tmp := strings.Split(s, "/")
	for _, v := range tmp {
		if v == ".." {
			return false
		}
	}
	return true
}
