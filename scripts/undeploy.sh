set -e

oc delete webeossites --all
oc delete -f deploy/crds/webeos_v1alpha1_webeossite_crd.yaml
oc delete project webeos
